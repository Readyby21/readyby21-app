/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

var DHR = {};

DHR.app = {
    blogData: [ {title: "TITLE1", body:"BODYLOL", id: "0"},
        {title: "TITLE2", body:"BODYLOL2", id: "1"}],

    eventTemplate: {},
    eventListTemplate: {},

    // Application Constructor
    initialize: function() {
        this.bindEvents();
        this.eventTemplate = Handlebars.compile($("#event-template").html());
        this.eventListTemplate = Handlebars.compile($("#eventDisplay-template").html());

    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
       // FastClick.attach(document.body);
    },


    homeBeforeCreate: function(event,args) {
       this.get_blog_data();
    },

    get_blog_data: function(){


        var app = this;
        var url = 'https://www.googleapis.com/calendar/v3/calendars/mpvndbjrpit95m1skpd55vejn8%40group.calendar.google.com/events?key=AIzaSyDR5DM4G-wmAcVOWPfc98ZYGGHVeLBAYbY';

        $.getJSON(url, function(data) {
            var li = '';
            for(i in data['items']) {
                item = data['items'][i];
                //$("#event-list").append(item.summary + "<br/>");
                app.blogData.push({title: item.summary, body: item.description, id: i});
                li += '<li><a  data-transition="slide" data-url="#post"data-val="'+i+'">'+item.summary+'</a></li>';


            }

            showData(li);

        });
        console.log(app.blogData);
        $("#eventListPageContent").html(app.eventListTemplate(app.blogData));
        console.log("BEFORE ENHANCE");
        //$("#eventListPageContent").enhanceWithin();
       //showData();
        console.log("AFTER ENHANCE");

        /*$.ajax({

            // The 'type' property sets the HTTP method.
            // A value of 'PUT' or 'DELETE' will trigger a preflight request.
            type: 'GET',

            // The URL to make the request to.
            url: 'https://www.googleapis.com/calendar/v3/calendars/mpvndbjrpit95m1skpd55vejn8%40group.calendar.google.com/events?key=AIzaSyDR5DM4G-wmAcVOWPfc98ZYGGHVeLBAYbY',

            // The 'contentType' property sets the 'Content-Type' header.
            // The JQuery default for this property is
            // 'application/x-www-form-urlencoded; charset=UTF-8', which does not trigger
            // a preflight. If you set this value to anything other than
            // application/x-www-form-urlencoded, multipart/form-data, or text/plain,
            // you will trigger a preflight request.
            contentType: 'text/plain',

            xhrFields: {
                // The 'xhrFields' property sets additional fields on the XMLHttpRequest.
                // This can be used to set the 'withCredentials' property.
                // Set the value to 'true' if you'd like to pass cookies to the server.
                // If this is enabled, your server must respond with the header
                // 'Access-Control-Allow-Credentials: true'.
                withCredentials: false
            },

            headers: {
                // Set any custom headers here.
                // If you set any non-simple headers, your server must include these
                // headers in the 'Access-Control-Allow-Headers' response header.
            },

            success: function(data) {
                // Here's where you handle a successful response.
                console.log("DATA:"+data);
                //console.log(JSON.stringify(data));
               // var responseData = JSON.parse(data.responseText);
                console.log("JSONED"+data.responseType);
                for(i in responseData['items']) {
                    item = ['items'][i];
                    //$("#event-list").append(item.summary + "<br/>");
                    console.log(item.summary);
                }
                app.blogData = $(data).find("items").map(function(i,item){
                    return {
                        //title: $(item).find("title").text(),
                        title: $(item).find("summary").text(),
                        body: $(item).find("description").text()
                    }
                }).toArray();
                console.log(app.blogData[0].title);
                $("#eventListPageContent").text(app.eventListTemplate(this.blogData));
                $("#eventListPageContent").listview().listview( "refresh" );

            },

            error: function() {
                // Here's where you handle an error response.
                // Note that if the error was due to a CORS issue,
                // this function will still fire, but there won't be any additional
                // information about the error.
                console.log("ERROR GETTING FEED");

            }
        });
*/
},
    postBeforeShow: function(event,args) {

        var post = this.blogData[args[1]];
        $("#post-content").html(this.eventTemplate(post));
        $("#post-content").enhanceWithin();
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};

DHR.router = new $.mobile.Router(
{
    "#post[?](\\d+)": {handler: "postBeforeShow", events:"bs"},
    "#eventListPage": {handler: "homeBeforeCreate", events:"bc"}

},
    DHR.app
);


function saveInfo()
{
    localStorage.setItem("county", "countyString");
    localStorage.setItem("age", "ageString");
}

function setCounty(str)
{
    localStorage.setItem("county", str)
    console.log("setting County");
}

function getCounty()
{
    //localStorage.getItem("county");
    document.getElementById("countyDisplay").innerHTML = localStorage.getItem("county");
    console.log("getting County");

}

function setAge(str)
{
    localStorage.setItem("age", str)
}

function getAge()
{
    //localStorage.getItem("age");
   // document.getElementById('ageDisplay').innerHTML = localStorage.getItem("age");
    $('#ageDisplay').text(localStorage.getItem("age"));
}

function displayEducation(age)
{
    var html;
    switch (age)
    {
        case "14":
            html= ['<div data-role="fieldcontain">',
            '<fieldset data-role="controlgroup">',
            '<input type="checkbox" name="checkbox-1" id="checkbox-1" class="custom" />',
            '<label for="checkbox-1">Begin participating in Life Skills Classes.</label>',
            '</fieldset>',
            ' <fieldset data-role="controlgroup">',
            '<input type="checkbox" name="checkbox-2" id="checkbox-2" class="custom" />',
            ' <label for="checkbox-2">Start getting your community service hours towards graduating high school.</label>',
            '</fieldset>',
            '<fieldset data-role="controlgroup">',
            '<input type="checkbox" name="checkbox-3" id="checkbox-3" class="custom" />',
        '<label for="checkbox-3">Make sure you know your school\'s graduation requirements.</label>',
                '  </fieldset>',
            '  </div>;'].join("");
            console.log(html);
            break;
        case "15":
            html = "<p>AHHAHAHAHA 15</p>"
            break;
        case "16":
            html = "<p>16</p>"
            break;


        default:
            console.log("error in age check");
            console.log(age);
            html = "<div><p>error</p></div>";
    }

    $("#educationContent").html(html);
}


function showData(li)
{
    if ( $('#eventDisplay').hasClass('ui-listview'))
    {

        $('#eventDisplay').empty();

        $('#eventDisplay').html(li);
        $('#eventDisplay').listview('refresh');

        console.log("HAS LISTVIEW");
    }
    else {
        $('#eventDisplay').trigger('create');
        console.log("CREATING LISTVIEW");
       // console.log( $('#eventDisplay').html());
       $('#eventDisplay').append(li);
      //  console.log( $('#eventDisplay').html());
      //  $('#eventDisplay').listview('refresh');
    }
}


$(document).on('vclick', '#eventDisplay', function(){
    DHR.app.blogData.id = $(this).attr('data-id');
    $.mobile.changePage( "#post", { transition: "slide", changeHash: false });
});

$("#age").on('change', function() {
    setAge( this.value );
});
$("#county").on('change', function() {
    setCounty( this.value );
});
$("#age").on('change', getAge);
$("#county").on('change', getCounty);

$("#educationLink").click(function() {
    displayEducation(localStorage.getItem("age") );

});



