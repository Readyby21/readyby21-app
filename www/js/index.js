/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/* FOR TESTING ONLY REMOVE LATER*/
localStorage.setItem("county", "allegany");
localStorage.setItem("age", "14");

var DHR = {};
var key;

DHR.app = {
    blogData: [],

    eventTemplate: [],
    eventListTemplate: [],

    // Application Constructor
    initialize: function () {
        this.bindEvents();
        this.eventTemplate = Handlebars.compile($("#singleEventPage-template").html());
        this.eventListTemplate = Handlebars.compile($("#eventDisplay-template").html());
        
        console.log("INITIALIZED");

    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function () {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function () {
        app.receivedEvent('deviceready');
        this.get_blog_data();
        
    },

    pageBeforeCreate: function (event, args) {
        this.get_blog_data();
    },

    get_blog_data: function () {
        var app = this;
        var d = new Date();

        //var today = d.getFullYear()+'-'+ d.getMonth()+1+'-'+d.getDate()+'T00:00:00-01:00';
		var today = d.toISOString();
        console.log("TODAY = "+today);

        var url = 'https://www.googleapis.com/calendar/v3/calendars/mpvndbjrpit95m1skpd55vejn8%40group.calendar.google.com/events?alwaysIncludeEmail=true&orderBy=startTime&singleEvents=true&timeMin='+today;
		key = 'AIzaSyDR5DM4G-wmAcVOWPfc98ZYGGHVeLBAYbY';
        var ua = navigator.userAgent.toLowerCase();
        var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
        if(isAndroid) {
            console.log("ITS AN ANDROID");
            key =  'AIzaSyCoAaF3Xy9nYwDlUZO2hiY29ZkgTakCnXc';
            }
    
    $.ajax({
          url: 'https://www.googleapis.com/calendar/v3/calendars/mpvndbjrpit95m1skpd55vejn8%40group.calendar.google.com/events?alwaysIncludeEmail=true&orderBy=startTime&singleEvents=true&timeMin='+today+'&key='+key,
          type: 'GET',
          dataType: 'json',
          success: function(data) 
          { 
	          	var li = '';
	            DHR.app.blogData.length = 0;
	            for (i in data['items']) {
	                item = data['items'][i];
	
	                DHR.app.blogData.push({
	                    title: item.summary,
	                    description: item.description
	                });
	                //$('#eventDisplay').append({title: item.summary, body: item.description});
	
	            }
	            DHR.app.blogData.push({
	                title: "SUCCESS TRIGGERED",
	                description: "FOR NOW"
	            });
	            //$('#eventDisplay').listview('refresh');
	            //showData(li);
		 		console.log("BEFORE SHOWDATA");
		        showData(DHR.app.eventListTemplate(DHR.app.blogData));
		        console.log("AFTER SHOWDATA");
		        $('#eventListPage').after(DHR.app.eventTemplate(DHR.app.blogData));
        	},
          error: function(data) 
          { 
          	console.log("ERROR GETTING JSON");
            console.log(data);
        	DHR.app.blogData.push({
                    title: data.responseText,
                    description: data.responseText
                });
                
              showData(DHR.app.eventListTemplate(DHR.app.blogData));
        	$('#eventListPage').after(DHR.app.eventTemplate(DHR.app.blogData));
        	
        	
          },
          beforeSend: setHeader
       
      });
     }  ,
      
      
    postBeforeShow: function (event, args) {

        var post = this.blogData[args[1]];
        $("#post-content").html(this.eventTemplate(post));
        $("#post-content").enhanceWithin();


    },
    // Update DOM on a Received Event
    receivedEvent: function (id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};


function setHeader(xhr) {
        
        xhr.setRequestHeader('key', key);
        gapi.client.setApiKey(key);
      }


function saveInfo() {
    localStorage.setItem("location", "countyString");
    localStorage.setItem("age", "ageString");
}

function setCounty(str) {
    localStorage.setItem("county", str)
    console.log("setting County");
}

function getCounty() {
    document.getElementById("countyDisplay").innerHTML = localStorage.getItem("county");
    console.log("getting County");
}

function setAge(str) {
    localStorage.setItem("age", str)
}

function getAge() {
    $('#ageDisplay').text(localStorage.getItem("age"));
}

function displayEducation(age) {
    var html;
    switch (age) {
        case "14":
            html = ['<div data-role="fieldcontain">',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="checkbox-1" id="checkbox-1" class="custom" />',
                '<label for="checkbox-1">Begin participating in Life Skills Classes.</label>',
                '</fieldset>',
                ' <fieldset data-role="controlgroup">',
                '<input type="checkbox" name="checkbox-2" id="checkbox-2" class="custom" />',
                ' <label for="checkbox-2">Start getting your community service hours towards graduating high school.</label>',
                '</fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="checkbox-3" id="checkbox-3" class="custom" />',
                '<label for="checkbox-3">Make sure you know your school\'s graduation requirements.</label>',
                '  </fieldset>',
                '  </div>'].join("");
            console.log(html);
            break;
        case "15":
        case "16":
            html = ['<div data-role="fieldcontain">',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="checkbox-11" id="checkbox-11" class="custom" />',
                '<label for="checkbox-11">Have a plan detailing how you&#39;re going to complete high school or obtain your GED.</label>',
                '</fieldset>',
                ' <fieldset data-role="controlgroup">',
                '<input type="checkbox" name="checkbox-12" id="checkbox-12" class="custom" />',
                ' <label for="checkbox-12">Participate in tutoring services (if necessary)</label>',
                '</fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="checkbox-13" id="checkbox-13" class="custom" />',
                '<label for="checkbox-13">Take a college tour or participate in a vocational/trade training program or school.</label>',
                '  </fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="checkbox-14" id="checkbox-14" class="custom" />',
                '<label for="checkbox-14">Know what is required to be admitted into a college or vocational program or school.</label>',
                '  </fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="checkbox-15" id="checkbox-15" class="custom" />',
                '<label for="checkbox-15">Participate in a Driver&#39;s Education School (if applicable)</label>',
                '  </fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="checkbox-16" id="checkbox-16" class="custom" />',
                '<label for="checkbox-16">Obtain your Learner&#39;s Permit and/or Maryland State Identification Card.</label>',
                '  </fieldset>',
                '  </div>'].join("");
            break;
        case "17":
        case "18":
            html = ['<div data-role="fieldcontain">',
                ' <fieldset data-role="controlgroup">',
                '<input type="checkbox" name="checkbox-21" id="checkbox-21" class="custom" />',
                ' <label for="checkbox-21">	 Have a plan for post-secondary education included in your transitional plan.</label>',
                '</fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="checkbox-22" id="checkbox-22" class="custom" />',
                '<label for="checkbox-22">Complete Free Application For Student Aid (FAFSA).</label>',
                '  </fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="checkbox-23" id="checkbox-23" class="custom" />',
                '<label for="checkbox-23">Enroll in college or a vocational program.</label>',
                '  </fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="checkbox-24" id="checkbox-24" class="custom" />',
                '<label for="checkbox-24">Understand service needs and know where to access services.</label>',
                '  </fieldset>',
                '<fieldset data-role="controlgroup">',
                '<input type="checkbox" name="checkbox-25" id="checkbox-25" class="custom" />',
                '<label for="checkbox-25">Register to vote and understand the voting process.</label>',
                '  </div>'].join("");
            break;
        case "19":
        case "21":
            html = ['<div data-role="fieldcontain">',
                ' <fieldset data-role="controlgroup">',
                '<input type="checkbox" name="checkbox-31" id="checkbox-31" class="custom" />',
                ' <label for="checkbox-31">Have access to post-secondary education supportive services.</label>',
                '  </div>'].join("");
            break;




        default:
            console.log("error in age check");
            console.log(age);
            html = "<div><p>error</p></div>";
    }

    $("#educationContent").empty();
    $("#educationContent").append(html).trigger('create');
    $("#educationContent").enhanceWithin();

}


function showData(li) {

    $('#eventDisplay').empty();
    $('#eventDisplay').append(li).trigger('create');

    //$('#eventDisplay').append(li);
    $( "#eventDisplay" ).listview().listview('refresh');
}

$(document).on('vclick', '#eventDisplay li a', function () {
	console.log("EVENT CLICKED");
    $.mobile.changePage("#singleEventPage?", {
        transition: "slide",
        changeHash: true
    });
});


$(document).on('vclick', '.linkToEventList', function () {
    console.log("EVENTLISTPAGE CLICKED");
    DHR.app.get_blog_data();
    $.mobile.changePage("#eventListPage", {
        transition: "slide",
        changeHash: true
    });
});

$("#eventListPage").on("pagecontainerbeforeshow", function (event, ui) {
    console.log("PAGEBEFORECHANGE");
    //DHR.app.get_blog_data();// get_blog_data();
});

$("#age").on('change', function () {
    setAge(this.value);
});
$("#location").on('change', function () {
    setCounty(this.value);
});
$("#age").on('change', getAge);
$("#location").on('change', getCounty);

$("#educationLink").click(function () {
    displayEducation(localStorage.getItem("age"));

});